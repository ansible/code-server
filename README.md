# VSCode in the browser

This Ansible role installs `code-server` using the provided DEB package from Github.

In essence, this role performs the following steps
(per the [code-server docs](https://coder.com/docs/code-server/latest/install#debian-ubuntu)):

```
curl -fOL https://github.com/coder/code-server/releases/download/v$VERSION/code-server_$VERSION_amd64.deb
sudo dpkg -i code-server_$VERSION_amd64.deb
sudo systemctl enable --now code-server@$USER
# Now visit http://127.0.0.1:8080. Your password is in ~/.config/code-server/config.yaml
```

This role also configures the `code-server` editor settings, and installs
your `code-server` extensions.
Depending on your Ansible playbook/hosts config, this role also configures
a reverse proxy Apache vhost with a Let's Encrypt certificate, and creates
a desktop entry.

Note that this role cannot handle downgrades of `code-server`. If you want to
downgrade you must uninstall manually, then set the version you want in this role.


## Remove code-server and all its settings

To wipe everything clean and start over (useful during testing of this role):

```
sudo apt remove --purge code-server
rm -rf ~/.local/share/code-server/
rm -rf ~/.config/code-server/
sudo systemctl stop code-server@taha.service
sudo rm /etc/systemd/system/default.target.wants/code-server@taha.service
sudo systemctl daemon-reload
```


## Browser support

I have observed and can confirm that the viewer windows that display R workspace objects
or plots work reliably in Chrome, but only for some types of objects in Chromium.
For this reason we probably want to run code-server in Chrome rather than Chromium.

First observed long ago, but still the case in Chrome 113.0.5672.92 and
Chromium 108.0.5359.40.

+ possibly [related code-server issue](https://github.com/coder/code-server/issues/1691) (closed but unresolved)


## Background research

+ https://underjord.io/the-best-parts-of-visual-studio-code-are-proprietary.html
+ https://reddit.com/r/programmingtools/comments/ddgf4b/theia_or_vs_code_for_opensource_ide_project/
+ https://dev.to/svenefftinge/theia-1-0-finally-a-good-browser-ide-3ok0
+ https://eclipsesource.com/blogs/2019/09/25/how-to-launch-eclipse-theia/
+ https://www.digitalocean.com/community/tutorials/how-to-set-up-the-eclipse-theia-cloud-ide-platform-on-digitalocean-kubernetes
+ https://www.digitalocean.com/community/tutorials/how-to-set-up-the-code-server-cloud-ide-platform-on-ubuntu-20-04
  This guide does not show how to auth using Linux PAM. Can we do that with `code-server`?
+ https://reposhub.com/javascript/editors/coderpair-vscode-live.html
  Possible to configure multiple user accounts, but still not PAM
+ http://www.codepool.info/2020/01/starting-live-server-of-vscode-in.html
+ https://tailscale.com/kb/1166/vscode-ipad/
  So we could set `auth: none` and put our own auth in front of `code-server`, e.g.,
  Keycloak at the reverse proxy.
+ https://www.dlford.io/docker-vscode-remote-development-server/
+ https://medium.com/@annantguptaneema/increase-productivity-with-vscode-server-2b5027e086ce


### Other Ansible roles

+ https://github.com/sr229/code-server-ansible
+ https://github.com/testcab/ansible-role-code-server
+ https://github.com/gantsign/ansible-role-visual-studio-code


### Could we run multiple concurrent instances of code-server?

[Coder](https://github.com/coder/coder)? Is that a solution? If so, is it not overkill?

+ [Creating instances of code server dynamically on demand #2558](https://github.com/coder/code-server/issues/2558)
+ https://github.com/coder/code-server/blob/v3.8.0/doc/FAQ.md#multi-tenancy
+ [Setting up code-server for multi-tenancy #792](https://github.com/coder/code-server/issues/792)
+ [Collaboration (multi user) support #33](https://github.com/coder/code-server/issues/33)
+ https://github.com/pojntfx/pojde
+ [Proper way to use code-server with multiple team members? #356](https://github.com/coder/code-server/issues/356)

My conclusion at present: no pret-a-porter solution exists, but should be
technically possible using Docker containers inside the LXC container, or else
multiple LXC containers, or simply different Linux user accounts on the single
container. None of these solutions are very elegant, though.


### Extension support in code-server

Some VSCode extensions explicitly mention that they do not support `code-server`,
for example:

+ https://github.com/James-Yu/LaTeX-Workshop/wiki/FAQ#code-server-is-not-supported
+ https://tex.stackexchange.com/questions/339/latex-editors-ides/390058#390058

`vscode-R` appears to support `code-server`, judging by issues/PRs related to
proxy support etc. [resolved in a recent release](https://github.com/REditorSupport/vscode-R/releases/tag/v2.3.1).

Can we get a nice `code-server` environment for LaTeX even without the
LaTeX Workshop extension? Can we work around it?
Some nice suggestions here https://pwsmith.github.io/2020/06/05/setting-up-a-text-editor-for-latex-vscode/

+ https://github.com/astoff/digestif
+ https://marketplace.visualstudio.com/search?term=latex&target=VSCode&category=All%20categories&sortBy=Relevance
+ https://marketplace.visualstudio.com/items?itemName=bnavetta.zoterolatex
  Navetta's extension is no longer maintained, and has been incorporated into LaTeX Utilities
+ https://github.com/tecosaur/LaTeX-Utilities



## Refs

+ https://coder.com/docs/code-server/latest/guide
+ https://github.com/coder/code-server/issues/282
+ https://rolkra.github.io/R-VSCode
+ https://schiff.co.nz/blog/r-and-vscode
+ [Setting up VS Code for R](https://gist.github.com/strengejacke/82e8d7b869bd9f961d12b4091c145b88)
+ https://github.com/coder/awesome-code-server
+ https://github.com/nullpo-head/Out-of-the-Box-CodeServer
